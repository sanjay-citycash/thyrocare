/* Website Name: CityCash
    Website URI: http://www.citycaash.in/
    Author: Mind Vision Infotech
    Author URI: http://www.mvinfotech.com
*/



(function($) {

    "use strict";


    $(window).load(function() {
        /* ------------------------------------- */
        /*   Preloader
         /* ------------------------------------- */
        $("#loading").delay(200).slideUp("slow");

        /* ------------------------------------- */
        /*   wow
         /* ------------------------------------- */
        new WOW().init();
    });






    //     $(window).bind('orientationchange resize', function(event){
    //       // alert();
    //       console.log(window.orientation)
    //   if (event.orientation) {
    //     if (event.orientation == 'landscape') {
    //       // console.log(window.rotation)
    //       if (window.rotation == 90) {
    //         // alert();
    //         rotate(this, -90);
    //        
    //       } else {
    //         rotate(this, 90);
    //        
    //       }
    //     }
    //   }
    // });

    // function rotate(el, degs) {
    //   iedegs = degs/90;
    //   if (iedegs < 0) iedegs += 4;
    //   transform = 'rotate('+degs+'deg)';
    //   iefilter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation='+iedegs+')';
    //   styles = {
    //     transform: transform,
    //     '-webkit-transform': transform,
    //     '-moz-transform': transform,
    //     '-o-transform': transform,
    //     filter: iefilter,
    //     '-ms-filter': iefilter
    //   };
    //   $(el).css(styles);
    // }



    /* ------------------------------------- */
    /*   Menu
     /* ------------------------------------- */
    $(window).on("scroll", function() {
        if ($(".header").hasClass("shrink")) {
            $(".logo-image").attr("src", "images/black_logo.png");
        } else {
            $(".logo-image").attr("src", "images/white_logo.png");
        }
    });

    // JS FOR ROTATION OF MOBILE:STARTS HERE
    window.addEventListener("orientationchange", function() {
        // alert("the orientation of the device is now " + screen.orientation.angle);
        if (screen.orientation.angle == 90) {
            $('body').hide();
        } else {
            $('body').show();
        }
    });
    // JS FOR ROTATION OF MOBILE:ENDS HERE


    // ON CLICK OF NAVIGATION SCROLL TO TOP CODE:STARTS HERE
    $(".logo").find("a").on("click", function() {
        // alert();
        $("html, body").animate({ scrollTop: 0 }, 700);
        return false;
    });
    //  ON CLICK OF NAVIGATION SCROLL TO TOP CODE:ENDS HERE

    // ON SCROLL PARTICULAR DRAWER TEXT IS SELECTED CODE:STARTS HERE
    $(window).on('scroll', function() {

        if ($(window).width() > 1301) {
            var scroller = $(window).scrollTop();
            // console.log(scroller)

            if (scroller <= 880) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.home').addClass('cd-selected');
            }

            if (scroller >= 881 && scroller <= 1500) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.modal-broken').addClass('cd-selected');
            }

            if (scroller >= 1500 && scroller <= 2200) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.worldcities').addClass('cd-selected');
            }

            if (scroller >= 2201 && scroller <= 2900) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.about').addClass('cd-selected');
            }

            if (scroller >= 2901 && scroller <= 3500) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.cityidentity').addClass('cd-selected');
            }

            if (scroller >= 3501 && scroller <= 4300) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.howwesolve').addClass('cd-selected');
            }

            if (scroller >= 4301 && scroller <= 5040) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.usecases').addClass('cd-selected');
            }

            if (scroller >= 5041 && scroller <= 5611) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.alliances').addClass('cd-selected');
            }

            if (scroller >= 5612 && scroller <= 6304) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.management').addClass('cd-selected');
            }

            if (scroller >= 6305 && scroller <= 9824) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.contact-us').addClass('cd-selected');
            }
        }

        if ($(window).width() >= 1000 && $(window).width() <= 1300) {
            var scroller = $(window).scrollTop();
            // console.log(scroller)

            if (scroller <= 700) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.home').addClass('cd-selected');
            }

            if (scroller >= 701 && scroller <= 1388) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.modal-broken').addClass('cd-selected');
            }

            if (scroller >= 1389 && scroller <= 2024) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.worldcities').addClass('cd-selected');
            }

            if (scroller >= 2025 && scroller <= 2700) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.about').addClass('cd-selected');
            }

            if (scroller >= 2701 && scroller <= 3358) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.cityidentity').addClass('cd-selected');
            }

            if (scroller >= 3359 && scroller <= 4100) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.howwesolve').addClass('cd-selected');
            }

            if (scroller >= 4101 && scroller <= 4800) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.usecases').addClass('cd-selected');
            }

            if (scroller >= 4801 && scroller <= 5393) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.alliances').addClass('cd-selected');
            }

            if (scroller >= 5394 && scroller <= 6090) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.management').addClass('cd-selected');
            }

            if (scroller >= 6092 && scroller <= 8000) {
                $(".cd-nav").find('li').removeClass('cd-selected');
                $(".cd-nav").find('.contact-us').addClass('cd-selected');
            }
        }

        //   if($(window).width() <= 1200){
        //     var scroller = $(window).scrollTop();
        //      // console.log(scroller)
        //    if(scroller <= 525){
        //       $(".cd-nav").find('li').removeClass('cd-selected');
        //       $(".cd-nav").find('.home').addClass('cd-selected');
        //    }

        //    if(scroller >= 526 && scroller <= 1617){
        //       $(".cd-nav").find('li').removeClass('cd-selected');
        //       $(".cd-nav").find('.modal-broken').addClass('cd-selected');
        //    }

        //    if(scroller >= 1618 && scroller <= 2576){
        //       $(".cd-nav").find('li').removeClass('cd-selected');
        //       $(".cd-nav").find('.worldcities').addClass('cd-selected');
        //    }

        //    if(scroller >= 2577 && scroller <= 3253){
        //       $(".cd-nav").find('li').removeClass('cd-selected');
        //       $(".cd-nav").find('.about').addClass('cd-selected');
        //    }

        //    if(scroller >= 3253 && scroller <= 3831){
        //       $(".cd-nav").find('li').removeClass('cd-selected');
        //       $(".cd-nav").find('.cityidentity').addClass('cd-selected');
        //    }

        //    if(scroller >= 3832 && scroller <= 4766){
        //       $(".cd-nav").find('li').removeClass('cd-selected');
        //       $(".cd-nav").find('.howwesolve').addClass('cd-selected');
        //    }

        //     if(scroller >= 4767 && scroller <= 5720){
        //       $(".cd-nav").find('li').removeClass('cd-selected');
        //       $(".cd-nav").find('.usecases').addClass('cd-selected');
        //    }

        //    if(scroller >= 5720 && scroller <= 7016){
        //       $(".cd-nav").find('li').removeClass('cd-selected');
        //       $(".cd-nav").find('.alliances').addClass('cd-selected');
        //    }

        //     if(scroller >= 7017 && scroller <= 8450){
        //       $(".cd-nav").find('li').removeClass('cd-selected');
        //       $(".cd-nav").find('.management').addClass('cd-selected');
        //    }

        //    if(scroller >= 8451 && scroller <= 9824){
        //       $(".cd-nav").find('li').removeClass('cd-selected');
        //       $(".cd-nav").find('.contact-us').addClass('cd-selected');
        //    }
        //   }

    });


    // TO ADD BOX SHADOW WHEN SHRINK CLASS IS ADDED TO HEADER:JS STARTS HERE
    $(".header").css("box-shadow", "none");
    $(window).on("scroll", function() {
        if ($(".header").hasClass("shrink")) {
            $(".header").css("box-shadow", "0 8px 6px -9px black");
        } else {
            $(".header").css("box-shadow", "none");
        }
    })
    // TO ADD BOX SHADOW WHEN SHRINK CLASS IS ADDED TO HEADER:JS ENDS HERE

    // ON SCROLL PARTICULAR DRAWER TEXT IS SELECTED CODE:ENDS HERE

    //open navigation clicking the menu icon
    $('.cd-nav-trigger').on('click', function(event) {
        event.preventDefault();
        toggleNav(true);
    });
    //close the navigation
    $('.cd-close-nav, .cd-nav, .cd-overlay').on('click', function(event) {
        event.preventDefault();
        toggleNav(false);
    });


    // close the drawer when clicked outside:starts here
    $(document).mouseup(function(e) {
        var container = $(".cd-nav-container");

        // console.log(container.has(e.target).length)  
        // console.log('zaid', container.is(e.target))
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            // console.log(container.length)
            e.preventDefault();
            toggleNav(false);
        }
    });
    // close the drawer when clicked outside:ends here




    //  hammer js for drawwer:starts here 
    var myElement = document.getElementById('nav');

    // create a simple instance
    // by default, it only adds horizontal recognizers
    var mc = new Hammer(myElement);

    // listen to events...
    mc.on("panleft panright tap press", function(ev) {
        // myElement.textContent = ev.type +" gesture detected.";
        if (ev.type == 'panleft') {
            // alert();
            event.preventDefault();
            toggleNav(false);
        }
    });
    // hammer js for drawwer:ends here



    //select a new section
    $('.cd-nav li').on('click', function(event) {
        var target = $(this),
            //detect which section user has chosen
            sectionTarget = target.data('menu');
        // console.log(sectionTarget)
        if (!target.hasClass('cd-selected')) {
            //if user has selected a section different from the one alredy visible
            //update the navigation -> assign the .cd-selected class to the selected item
            target.addClass('cd-selected').siblings('.cd-selected').removeClass('cd-selected');
            //load the new section
            loadNewContent(sectionTarget);
        } else {
            // otherwise close navigation
            toggleNav(false);
        }
    });



    function toggleNav(bool) {
        $('.cd-nav-container, .cd-overlay').toggleClass('is-visible', bool);
        $('main').toggleClass('scale-down', bool);
    }

    function loadNewContent(newSection) {
        //create a new section element and insert it into the DOM
        var section = $('<section class="cd-section ' + newSection + '"></section>').appendTo($('main'));
        //load the new content from the proper html file
        section.load(newSection + '.html .cd-section > *', function(event) {
            //add the .cd-selected to the new section element -> it will cover the old one
            section.addClass('cd-selected').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
                //close navigation
                toggleNav(false);
            });
            section.prev('.cd-selected').removeClass('cd-selected');
        });

        $('main').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
            //once the navigation is closed, remove the old section from the DOM
            section.prev('.cd-section').remove();
        });

        if ($('.no-csstransitions').length > 0) {
            //if browser doesn't support transitions - don't wait but close navigation and remove old item
            toggleNav(false);
            section.prev('.cd-section').remove();
        }
    }

    /* Menu color effect */
    $(window).scroll(function() {
        var navbarHeight = $('.hero').outerHeight();
        if ($(this).scrollTop() > navbarHeight && !$('.navigation').hasClass('nav-up')) {
            $('.navigation').addClass('nav-up');
        } else if ($(this).scrollTop() <= navbarHeight) {
            $('.navigation').removeClass('nav-up');
        }
    });



    /* Smooth scroll function */
    $(document).on('click', 'ul.cd-nav li a', function(e) {
        if ($(e.target).is('a[href*="#"]:not([href="#"]')) {
            // console.log("Test1");
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (window.innerWidth <= 800) {
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 120
                        }, 1000);
                        return false;
                    }
                } else {
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 60
                        }, 1000);
                        return false;
                    }
                }
            }
        }
        else{
            window.open(this.getAttribute('href'));
            if (window.innerWidth <= 800 && window.innerHeight <= 600) {
                    
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top - 180
                    }, 1000);
                    return false;
                }
            }
        }
    });




    /* ------------------------------------- */
    /*  Contact form functions
     /* ------------------------------------- */
    /* E-mail validation via regular expression */
    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    };

    $(function() {
        $("#contactform").on('submit', function(event) {

            var input = $('.message-contactform');

            if (!input.is(':empty')) {
                input.stop(true);
            }
            event.preventDefault();
            event.stopImmediatePropagation();

            var name = $("input#contact-name");
            var email = $("input#contact-email");
            var title = $("input#contact-title");
            var message = $("textarea#contact-message");

            if (name.val() == "" || email.val() == "" || title.val() == "" || message.val() == "") {
                input.stop(true).html('<i class="fa fa-warning"></i> All fields are required.');
                $("#contactform").find("input[type=text],textarea").filter(function() {
                    if ($(this).val() == "") {
                        event.preventDefault();
                        return true;
                    }
                }).first().focus();
            } else if (!isValidEmailAddress(email.val())) {
                input.stop(true).html('<i class="fa fa-warning"></i> E-mail address is not valid.');
                email.focus();
            } else {
                $.ajax({
                    type: "POST",
                    url: "./assets/php/send-contact.php",
                    data: {
                        contact_name: name.val(),
                        contact_email: email.val(),
                        contact_title: title.val(),
                        contact_message: message.val()
                    },
                    success: function() {
                        input.html('<i class="fa fa-check"></i> Thank you for your message!');
                        name.val('');
                        email.val('');
                        title.val('');
                        message.val('');
                    }
                });
            }
        });
    });

    /* ------------------------------------- */
    /*  Newsletter
     /* ------------------------------------- */
    $("#notifyMe").notifyMe();


    $(document).ready(function() {

        /* ------------------------------------- */
        /*   Animated progress bars
         /* ------------------------------------- */
        $('.progress-bars').waypoint(function() {
            $('.progress').each(function() {
                $(this).find('.progress-bar').animate({
                    width: $(this).attr('data-percent')
                }, 200);
            });
        }, {
            offset: '100%',
            triggerOnce: true
        });

        /* ------------------------------------- */
        /*  counter
         /* ------------------------------------- */
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });

        /* ------------------------------------- */
        /*  owl-carousel
         /* ------------------------------------- */
        $(".owl-about").owlCarousel({
            autoplay: false,
            stopOnHover: true,
            dots: true,
            responsive: {
                0: {
                    items: 1,
                    dots: true
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });

        $(".owl-clients").owlCarousel({
            items: 1,
            autoplay: true,
            stopOnHover: true,
            dots: true,
        });

        $(".client").owlCarousel({
            autoPlay: 4000,
            margin: 10,
            stopOnHover: true,
            dots: true,
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 2
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 3
                }
            }
        });

        $(".news").owlCarousel({
            autoPlay: false,
            stopOnHover: true,
            dots: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 4
                }
            }
        });

        $(".slider-wrapper").owlCarousel({
            singleItem: true,
            loop: true,
            nav: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            navText: ['<span class="prev"></span>', '<span class="next"></span>'],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });
        /* ------------------------------------- */
        /*   portfolio-filter
         /* ------------------------------------- */
        // filter items on button click
        $('.portfolio-filter').on('click', 'a', function(e) {
            e.preventDefault();
            var filterValue = $(this).attr('data-filter');
            $container.isotope({
                filter: filterValue
            });
            $('.portfolio-filter a').removeClass('active');
            $(this).closest('a').addClass('active');
        });


        // isotope Masonry
        var $container = $('.masonry');
        $container.imagesLoaded(function() {
            $container.isotope({
                itemSelector: '.masonry-item',
                layoutMode: 'masonry',
                resizesContainer: false,
                percentPosition: true,
                masonry: {
                    columnWidth: '.work-img',
                    gutter: 6
                }
            });
        });

        $('.masonry-posts').isotope({
            masonry: {
                itemSelector: '.post',
                percentPosition: true,
                gutter: 15
            }
        });

        /* ------------------------------------- */
        /*   pricing table
         /* ------------------------------------- */
        $('.pricing').waypoint(function() {
            $('.pricing-best').addClass('depth');
        });


        /* ------------------------------------- */
        /*  Lightbox popup
         /* ------------------------------------- */
        $('.lightbox-gallery').magnificPopup({
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1]
            },
            image: {
                titleSrc: 'title',
                verticalFit: true
            }
        });

        $('.lightbox-video').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false,
            iframe: {
                patterns: {
                    youtube: {
                        index: 'youtube.com/',
                        id: 'v=',
                        src: 'http://www.youtube.com/embed/%id%?autoplay=1'
                    }
                }
            }
        });

        /* ------------------------------------- */
        /*   google map 
         /* ------------------------------------- */
        if($("#map-canvas").length){
            var map;
            // var myCenter=new google.maps.LatLng(51.316098, 9.481401);
            var myCenter = new google.maps.LatLng(19.1178585, 72.8692754);
            var marker = new google.maps.Marker({
                icon: "assets/images/map_icon.png",
                position: myCenter
            });


            function initialize() {
                var mapProp = {
                    center: myCenter,
                    zoom: 14,
                    draggable: false,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    styles: [{
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }]
                    }, {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }]
                    }, {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }]
                    }, {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }]
                    }, {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }]
                    }, {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [{ "color": "#dedede" }, { "lightness": 21 }]
                    }, {
                        "elementType": "labels.text.stroke",
                        "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }]
                    }, {
                        "elementType": "labels.text.fill",
                        "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }]
                    }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }]
                    }, {
                        "featureType": "administrative",
                        "elementType": "geometry.fill",
                        "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }]
                    }, {
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }]
                    }]
                };

                map = new google.maps.Map(document.getElementById("map-canvas"), mapProp);
                marker.setMap(map);

                google.maps.event.addListener(marker, 'click', function() {

                    infowindow.setContent(contentString);
                    infowindow.open(map, marker);

                });
            };
            google.maps.event.addDomListener(window, 'load', initialize);

            google.maps.event.addDomListener(window, "resize", resizingMap());

            $('#map-popup').on('show.bs.modal', function() {
                resizeMap();
            })

            function resizeMap() {
                if (typeof map == "undefined") return;
                setTimeout(function() { resizingMap(); }, 400);
            }

            function resizingMap() {
                if (typeof map == "undefined") return;
                var center = map.getCenter();
                google.maps.event.trigger(map, "resize");
                map.setCenter(center);
            }
        }
        /* ------------------------------------- */
        /*  bgndVideo hero
         /* ------------------------------------- */
        jQuery("#bgndVideo").YTPlayer();

        /*---------------------------------------*/
        /*  Tweeter feed
         /*--------------------------------------*/
        $('.tweet').twittie({
            username: 'EnvatoStudio',
            hideReplies: true,
            dateFormat: '%b. %d, %Y',
            template: '{{screen_name}} {{tweet}} <div class="date">{{date}}</div>',
            count: 1,
            loadingText: 'Loading!'
        });



    });
})(jQuery);