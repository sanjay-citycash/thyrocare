var en = {
            "ms-h1": "&ldquo;Health checkup today for healthier life tomorrow&rdquo;",
            "ms-h2": "Health Screen M",
            "ms-h3": "TEST INCLUDED",
            "ms-li1": "COMPLETE HEMOGRAM",
            "ms-li2": "DIABETES",
            "ms-li3": "THYROID",
            "ms-btn": "SEE DETAILS",
            "as-h5": "How to avail",
            "as-h6": "TEST",
            "as-l1": "Visit nearby ROKAD Counter",
            "as-lc1": "Offer is available only with ROKAD agent",
            "as-l2": "Recharge your ST Smart Card",
            "as-lc2": "Recharge your <b>ST chya Wallet</b> Ask the operator to recharge the wallet with Rs 100 or more.",
            "as-l3": "Receive Code on Registered Mobile No.",
            "as-lc3": "<b>Receive Code</b> A code will be sent on the registered mobile number.",
            "as-l4": "Call Thyrocare number and book your test",
            "as-lc4": "<b>Book your test</b> Call thyrocare support number and book the date & time of your choice for sample collection",
            "as-no": "<a href='tel:02230900000' target='_blank'>022 - 309 000 00</a>",
            "as-ia": "In association with",
            "as-l5": "Test Sample will be collected at your home",
            "as-lc5": "Sample Collection: An Operator will visit your home to collect the sample.",
            "as-l6": "Collect Test Reports from Agent Or Get it through Courier",
            "as-lc6": "Test Report: You can get your report on the email ID provided. or You can ask for the hard copy to be sent on your address.",
            "as-p1": "Minimum 20% off on other tests, book now",
            "as-p2": "For Terms & Conditions please visit <a href='http://www.citycash.in/tnc' target='_blank'><u>www.citycash.in/tnc</u></a>",
            "ts-h5": "List of test",
            "ts-h6": "On Offer",
            "ts-l1": "AAROGYAM 1.2",
            "ts-l2": "AAROGYAM B",
            "ts-l3": "ADVANCED RENAL PROFILE",
            "ts-l4": "ARTHRITIS PROFILE",
            "ts-l5": "CARDIAC PROFILE",
            "ts-l6": "Diabetic Profile - M",
            "ts-l7": "FEVER PROFILE",
            "ts-l8": "HEPATITIS B PROFILE",
            "ts-l9": "IRON DEFICIENCY PROFILE",
            "ts-l10": "LIPID PROFILE",
            "ts-l11": "LIVER FUNCTION TESTS",
            "ts-l12": "PALEO PROFILE",
            "ts-l13": "T3-T4-TSH",
            "ts-l14": "VITAMIN D PROFILE",
            "ts-l15": "HEALTHSCREEN - M",
            "ts-l16": "Advanced Thyroid Profile"
        };

var en_img = {
    "ts-l1": "assets/images/plans/aarogyam-12.png",
    "ts-l2": "assets/images/plans/aarogyam-b.png",
    "ts-l3": "assets/images/plans/advanced-renal-profile.png",
    "ts-l4": "assets/images/plans/arthritis-profile-m.png",
    "ts-l5": "assets/images/plans/cardiac-profile-m.png",
    "ts-l6": "assets/images/plans/diabetic-profile-m.png",
    "ts-l7": "assets/images/plans/fever_profile.png",
    "ts-l8": "assets/images/plans/hepatitis-b-profile.png",
    "ts-l9": "assets/images/plans/iron-deficiency-profile.png",
    "ts-l10": "assets/images/plans/lipid-profile.png",
    "ts-l11": "assets/images/plans/liver-function-tests.png",
    "ts-l12": "assets/images/plans/paleo-profile-m.png",
    "ts-l13": "assets/images/plans/T3-T4-TSH.png",
    "ts-l14": "assets/images/plans/vitamin-d-profile.png",
    "ts-l15": "assets/images/plans/Healthscreen M_CTCV_NEW1.jpg",
    "ts-l16": "assets/images/plans/advanced-thyroid-profile.png"
}
var mr = {
            "ms-h1": "&ldquo;उद्याच्या आरोग्यदायी जीवनासाठी  आजच आरोग्य तपासणी करा&rdquo;",
            "ms-h2": "योजनेचे नाव: हेल्थ स्क्रीन - एम",
            "ms-h3": "चाचणी समाविष्ट",
            "ms-li1": "कंप्लीट हेमोग्राम",
            "ms-li2": "डायबेटिस",
            "ms-li3": "थायरॉईड",
            "ms-btn": "तपशील बघा",
            "as-h5": "कसा फायदा होईल",
            "as-h6": "टेस्ट",
            "as-l1": "जवळील रोकड काउंटरला भेट द्या",
            "as-lc1": "ऑफर फक्त रोकाड एजंटसाठी उपलब्ध आहे",
            "as-l2": "आपले एसटी स्मार्टकार्ड रिचार्ज करा",
            "as-lc2": "आपले एसटी वॉलेट रिचार्ज करा ऑपरेटरला 100 किंवा त्याहून अधिक रुपयांचे पाकीट रिचार्ज करण्यास सांगा",
            "as-l3": "कूपन कोड एसएमएस वर मिळवा",
            "as-lc3": "कोड प्राप्त करा नोंदणीकृत मोबाइल नंबरवर पाठविला जाईल.",
            "as-l4": "तपासणीसाठी आपली अपॉईंटमेंट बुक करा",
            "as-lc4": "आपली चाचणी बुक करा थायरोकेअर समर्थन क्रमांकावर कॉल करा आणि नमुना संकलनासाठी आपल्या आवडीची तारीख आणि वेळ बुक करा.",
            "as-no": "<p>कृपया चाचणी बुक करण्यासाठी</p><a href='tel:02230900000' target='_blank'>०२२ ३०९० ००००</a><p>वर कॉल करा</p>",
            "as-ia": "संयुक्त विद्यमाने",
            "as-l5": "नमुना संग्रह: एक ऑपरेटर नमुना गोळा करण्यासाठी आपल्या घरी भेट देईल.",
            "as-lc5": "चाचणी नमुना आपल्या घरी गोळा केला जाईल",
            "as-l6": "एजंटकडून आपला रिपोर्ट गोळा करा",
            "as-lc6": "चाचणी अहवाल: प्रदान केलेल्या ईमेल आयडीवर आपला अहवाल मिळू शकेल. किंवा आपण आपल्या पत्त्यावर हार्ड कॉपी पाठविण्यासाठी विचारू शकता.",
            "as-p1": "कमीत कमी २०% सवलतीत इतर चाचण्या करण्यासाठी बुक करा",
            "as-p2": "अधिक माहिती साठी <a href='http://www.citycash.in/tnc' target='_blank'><u>www.citycash.in/tnc</u></a> साईट वर विझिट करा",
            "ts-h5": "चाचणी यादी",
            "ts-h6": "ऑफर वर",
            "ts-l1": "आरोगायम १.२",
            "ts-l2": "आरोगम ब",
            "ts-l3": "अ‍ॅडव्हान्सड रिनल प्रोफाइल",
            "ts-l4": "आर्ट्रिटिस प्रोफाइल",
            "ts-l5": "कॅरिएक प्रोफाईल",
            "ts-l6": "मधुमेह प्रोफाइल - एम",
            "ts-l7": "फेवर प्रोफाइल",
            "ts-l8": "हेपेटाइटिस बी प्रोफाइल",
            "ts-l9": "लोह अचूकता प्रोफाइल",
            "ts-l10": "लिपिड प्रोफाइल",
            "ts-l11": "लाइव्ह फंक्शन टेस्ट",
            "ts-l12": "पॅलेओ प्रोफाइल",
            "ts-l13": "टी 3-टी 4-टीएसएच",
            "ts-l14": "व्हिटॅमिन डी प्रोफाइल",
            "ts-l15": "आरोग्य आरोग्य - एम",
            "ts-l16": "प्रगत थायरॉईड प्रोफाइल"
}

var mr_img = {
    "ts-l1": "assets/images/plans/marathi/aarogyam-12.jpg",
    "ts-l2": "assets/images/plans/marathi/aarogyam-b.jpg",
    "ts-l3": "assets/images/plans/marathi/advanced-renal-profile.jpg",
    "ts-l4": "assets/images/plans/marathi/arthritis-profile-m.jpg",
    "ts-l5": "assets/images/plans/marathi/cardiac-profile-m.jpg",
    "ts-l6": "assets/images/plans/marathi/diabetic-profile-m.jpg",
    "ts-l7": "assets/images/plans/marathi/fever_profile.jpg",
    "ts-l8": "assets/images/plans/marathi/hepatitis-b-profile.jpg",
    "ts-l9": "assets/images/plans/marathi/iron-deficiency-profile.jpg",
    "ts-l10": "assets/images/plans/marathi/lipid-profile.jpg",
    "ts-l11": "assets/images/plans/marathi/liver-function-tests.jpg",
    "ts-l12": "assets/images/plans/marathi/paleo-profile-m.jpg",
    "ts-l13": "assets/images/plans/marathi/T3-T4-TSH.jpg",
    "ts-l14": "assets/images/plans/marathi/vitamin-d-profile.jpg",
    "ts-l15": "assets/images/plans/marathi/Healthscreen M_CTCV_NEW1.jpg",
    "ts-l16": "assets/images/plans/marathi/advanced-thyroid-profile.jpg"
}

function changeLang(lang){
    var obj = {};
    var lang_img = {};
    var html = "";
    if(lang == "MAR"){
        obj = mr;
        lang_img = mr_img;
        html = "<button type='button' onclick=\"changeLang('ENG')\">English</button>";
    } else {
        obj = en;
        lang_img = en_img;
        html = "<button type='button' onclick=\"changeLang('MAR')\">मराठी</button>";
    }
    
    $("#lang-a").html(html);

    $.each(obj, function(key, val){
        $('*[data-lang="'+key+'"').html(val)
    });

    $.each(lang_img, function(key, val){
        $('*[data-img="'+key+'"').attr("href", val);
        $('*[data-img="'+key+'"').children("img").attr("src", val);
    });
}

$(document).ready(function(){
    changeLang("MAR")
})